﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
	[SerializeField] float hitPoints = 100f;

	public void TakeDamage(float damage)
	{
		hitPoints -= damage;
		Debug.Log(hitPoints);
		if (hitPoints <= 0)
		{
			GetComponent<DeathHandler>().HandlerDeath();
		}
	}
}
