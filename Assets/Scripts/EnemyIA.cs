﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyIA : MonoBehaviour
{
	[SerializeField] Transform target;
	[SerializeField] float chaseRange = 5f;
	[SerializeField] float turnSpeed = 5f;

	NavMeshAgent navMeshAgent;
	Animator anim;

	float distanceToTarget = Mathf.Infinity;
	bool isProvoked;

	void Start()
    {
		navMeshAgent = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator>();
    }

    void Update()
    {
		distanceToTarget = Vector3.Distance(target.position, transform.position);

		if (isProvoked)
		{
			EngageTarget();
		} else if (distanceToTarget <= chaseRange) // Colocou-se esse if em baixo para não ficar verificando todo frame a condição.
		{
			isProvoked = true;
		}
	}

	private void EngageTarget()
	{
		FaceTarget();
		if (distanceToTarget >= navMeshAgent.stoppingDistance)
		{
			ChaseTarget();
		}

		if (distanceToTarget <= navMeshAgent.stoppingDistance)
		{
			AttackTarget();
		}

		if(distanceToTarget >= chaseRange)
		{
			anim.SetTrigger("idle");
			isProvoked = false;
		}
	}

	private void AttackTarget()
	{
		anim.SetBool("attack", true);
	}

	private void ChaseTarget()
	{
		anim.SetBool("attack", false);
		anim.SetTrigger("chase");
		navMeshAgent.SetDestination(target.position);
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, chaseRange);
	}

	private void FaceTarget()
	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed);
	}
}
