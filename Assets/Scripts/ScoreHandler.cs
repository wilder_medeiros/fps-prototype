﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreHandler : MonoBehaviour
{
	[SerializeField] TextMeshProUGUI textMesh;

	[SerializeField] int enemyScoreAmount;

	private void Start()
	{
		textMesh.text = "0";
	}
}
