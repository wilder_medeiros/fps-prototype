﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
	[SerializeField] float range = 100f;
	[SerializeField] float damage = 20f;

	[SerializeField] Camera FPCamera;
	[SerializeField] ParticleSystem muzzleFlash;
	[SerializeField] GameObject hitEffect;
	[SerializeField] AudioClip clip;

	AudioSource audioSource;

	private void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}

	void Update()
    {
		if (Input.GetButtonDown("Fire1"))
		{
			Shoot();
		}
    }

	private void Shoot()
	{
		PlaySoundEffect();
		PlayMuzzleEffect();
		ProcessRayCast();
	}

	private void PlaySoundEffect()
	{
		if (!PauseHandler.gameIsPaused)
		{
			AudioSource.PlayClipAtPoint(clip, transform.position);
		}
	}

	private void PlayMuzzleEffect()
	{
		if (!PauseHandler.gameIsPaused)
		{
			muzzleFlash.Play();
		}
	}

	private void ProcessRayCast()
	{
		RaycastHit hit;
		if (Physics.Raycast(FPCamera.transform.position, FPCamera.transform.forward, out hit, range))
		{
			Debug.Log(hit.transform.name);

			CreateHitImpact(hit);
			EnemyHealth target = hit.transform.GetComponent<EnemyHealth>();
			if (target == null) return;
			target.TakeDamage(damage);
		}
		else
		{
			return;
		}
	}

	private void CreateHitImpact(RaycastHit hit)
	{
		if (!PauseHandler.gameIsPaused)
		{
			GameObject impact = Instantiate(hitEffect, hit.point, Quaternion.LookRotation(hit.normal));
			Destroy(impact, 1f);
		}
	}
}
