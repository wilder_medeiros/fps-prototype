﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseHandler : MonoBehaviour
{
	[SerializeField] Canvas pauseGameCanvas;

	public static bool gameIsPaused = false;

	private void Start()
	{
		pauseGameCanvas.enabled = false;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.F1))
		{
			if (gameIsPaused)
			{
				Resume();
			}
			else
			{
				Pause();
			}

		}
	}

	private void Resume()
	{
		pauseGameCanvas.enabled = false;
		Time.timeScale = 1;
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = false;
		gameIsPaused = false;
	}

	private void Pause()
	{
		pauseGameCanvas.enabled = true;
		Time.timeScale = 0;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		gameIsPaused = true;

	}
}
